package by.stormnet.fxfigures.client.controller;

import by.stormnet.fxfigures.client.ClientApp;
import by.stormnet.fxfigures.commons.geometry.Circle;
import by.stormnet.fxfigures.commons.geometry.Figure;
import by.stormnet.fxfigures.commons.geometry.Rectangle;
import by.stormnet.fxfigures.commons.geometry.Triangle;
import by.stormnet.fxfigures.commons.messaging.Message;
import by.stormnet.fxfigures.commons.messaging.MessageDispatcher;
import by.stormnet.fxfigures.commons.threading.ClientThread;
import by.stormnet.fxfigures.commons.threading.IClientThread;
import javafx.application.Platform;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.canvas.Canvas;
import javafx.scene.input.MouseEvent;
import javafx.scene.paint.Color;

import java.io.*;
import java.net.InetAddress;
import java.net.Socket;
import java.net.URL;
import java.util.*;

public class MainViewController<T extends Thread & IClientThread> implements Initializable, Serializable, MessageDispatcher<T> {
    private LinkedList<Figure> figures;
    private Random rnd;
    private ClientThread clientThread;

    @FXML
    private Canvas canvas;

    public MainViewController() {
        figures = new LinkedList<>();
        rnd = new Random(System.currentTimeMillis());
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        try {
            Socket socket = new Socket(InetAddress.getByName("localhost"), 9999);
            clientThread = new ClientThread(socket);
            registerClientThread(null);
        } catch (IOException e) {
            e.printStackTrace();
        }
        //clientThread

    }

    @FXML
    private void clickOnButton(MouseEvent event) {
        printTheArr(figures);
        printTheSquare(figures);


    }

    public void clickOnSORTButton(MouseEvent mouseEvent) {
        figures.addAll(Deserializacia());
        repaint();

        sortTheFigures(figures);


    }

    private void sortTheFigures(LinkedList<Figure> figures) {
        TreeSet<Figure> f = new TreeSet<>(new Comparator<Figure>(){

            @Override
            public int compare(Figure o1, Figure o2) {
                return o1.getType() - o2.getType();
            }
        }.thenComparing(new Comparator<Figure>() {
            @Override
            public int compare(Figure o1, Figure o2) {
                return (int)(o1.printSquare() - o2.printSquare());
            }
        }));
        f.addAll(figures);
        for (Figure figure : f) {
            //System.out.println(figure.toString());
            figure.printInfo();
        }

    }

    private void printTheSquare(LinkedList<Figure> figures) {
        double sq = 0;
        for (Figure figure : figures) {
            sq += figure.printSquare();
        }
        System.out.printf("Total square = %.3f", sq);
    }

    @FXML
    private void onMouseClicked(MouseEvent event) {
        System.out.println("x: " + event.getX());
        System.out.println("y: " + event.getY());
        Message<Figure> message = new Message<>(createFigure(event.getX(),event.getY()));
        if (clientThread != null){
            try {
                clientThread.sendMessage(message);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        //figures.add(createFigure(event.getX(), event.getY()));
        Serializacia(figures);
        repaint();


    }

    private void printTheArr(LinkedList<Figure> f){
        for (Figure figure : figures) {
            figure.printInfo();
        }
        System.out.println("=======================================================");
    }



    /*private void addFigure(Figure figure){
        if (figures == null)
            figures = new LinkedList<>();
        if (figures.getLast() == null){
            figures.add(figure);
            return;
        }

        figures.add(figure);*/

      /*  Figure[] tmp = new Figure[figures.length + 1];
        int index = 0;
        for(; index < figures.length; index++){
            tmp[index] = figures[index];
        }
        tmp[index] = figure;
        figures = tmp;*/

 //   }

    private Figure createFigure(double x, double y){
        Figure figure = null;
        int type = rnd.nextInt(3);
        switch(type){
            case Figure.FIGURE_TYPE_CIRCLE:
                System.out.println("create circle");
                double lineWidthCircle = rnd.nextInt(6);
                double radius = rnd.nextInt(101);
                figure = new Circle(x, y, lineWidthCircle == 0 ? 1 : lineWidthCircle, Color.RED,
                        radius < 10 ? 10 : radius);
                break;
            case Figure.FIGURE_TYPE_RECTANGLE:
                System.out.println("create rectangle");
                double lineWidthRectangle = rnd.nextInt(6);
                double width = rnd.nextInt(101);
                double height = rnd.nextInt(101);
                figure = new Rectangle(x, y, lineWidthRectangle == 0 ? 1 : lineWidthRectangle, Color.CORNFLOWERBLUE,
                        width < 10 ? 10 : width, height < 10 ? 10 : height );
                break;
            case Figure.FIGURE_TYPE_TRIANGLE:
                System.out.println("create triangle");
                double lineWidthTriangle = rnd.nextInt(6);
                double base = rnd.nextInt(101);
                figure = new Triangle(x, y, lineWidthTriangle == 0 ? 1 : lineWidthTriangle, Color.LIME,
                        base < 10 ? 10 : base);
                break;
            default:
                System.out.println("Unknown figure type.");

        }

        return figure;
    }

    private void repaint(){
        if(figures == null)
            return ;
        canvas.getGraphicsContext2D().clearRect(0,0, ClientApp.SCREEN_WIDTH, ClientApp.SCREEN_HEIGHT);
        for (Figure figure : figures){
            if (figure != null){

                figure.draw(canvas.getGraphicsContext2D());

            }
        }

    }


    private void Serializacia(LinkedList<Figure> figures){
        try {
            ObjectOutputStream oos = new ObjectOutputStream(new BufferedOutputStream(new FileOutputStream("figuresSave.bin")));
            oos.writeObject(figures);
            oos.close();

        }
        catch (IOException e) {
            e.printStackTrace();
        }
    }

/*    private void initFigures(){
        if (){

        }
    }*/

    private LinkedList<Figure> Deserializacia(){
        LinkedList<Figure> tmp = null;
        try {
            ObjectInputStream ois = new ObjectInputStream(new BufferedInputStream(new FileInputStream("figuresSave.bin")));
             tmp =(LinkedList<Figure>) ois.readObject();
            System.out.println(tmp);

        } catch (IOException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
      return tmp;

    }

    @Override
    public void registerClientThread(T client) {
        clientThread.setClientThreadListener(this);
        clientThread.start();
    }

    @Override
    public void unregisterClienThread(T client) {

    }

    @Override
    public <M extends Message> void dispatchMessage(M message) {
        Figure figure = message.getFigure();
        switch (figure.getType()){
            case Figure.FIGURE_TYPE_RECTANGLE:
                figure.setColor(Color.CORNFLOWERBLUE);
                break;
            case Figure.FIGURE_TYPE_CIRCLE:
                figure.setColor(Color.BLACK);
                break;
            case Figure.FIGURE_TYPE_TRIANGLE:
                figure.setColor(Color.RED);
                break;
            default:
                System.out.println("Unknown figure type!");
        }

        figures.add(figure);
        Platform.runLater(() -> repaint());

    }

    @Override
    public void clear() {
        clientThread.stopThread();
    }
}
