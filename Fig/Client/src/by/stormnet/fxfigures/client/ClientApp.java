package by.stormnet.fxfigures.client;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.stage.Stage;

public class ClientApp extends Application {
    public static final double SCREEN_WIDTH = 1280;
    public static final double SCREEN_HEIGHT = 800;

    public static void main(String[] args) {
        launch();
    }

    @Override
    public void start(Stage wnd) throws Exception {
        wnd.setTitle("Fx Client App");
        wnd.setScene(new Scene(FXMLLoader.load(getClass().getResource("/views/MainView.fxml")), SCREEN_WIDTH, SCREEN_HEIGHT));
        wnd.show();

    }
}
