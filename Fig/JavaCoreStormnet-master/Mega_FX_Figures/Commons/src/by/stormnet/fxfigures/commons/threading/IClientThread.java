package by.stormnet.fxfigures.commons.threading;

import by.stormnet.fxfigures.commons.messaging.Message;
import by.stormnet.fxfigures.commons.messaging.MessageDispatcher;

import java.io.IOException;

public interface IClientThread {
    void setClientThreadListener(MessageDispatcher listener);
    <M extends Message> void sendMessage(final M message) throws IOException;
    void stopThread();
}
