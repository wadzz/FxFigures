package by.stormnet.fxfigures.commons.geometry;

public interface Squerable {
    double printSquare();
}
