package by.stormnet.fxfigures.commons.messaging;

import by.stormnet.fxfigures.commons.geometry.Figure;

import java.io.Serializable;

public class Message<T extends Figure> implements Serializable {
    private static final long serialVersionUID = 1L;
    private T figure;

    public Message(T figure) {
        this.figure = figure;
    }

    public T getFigure() {
        return figure;
    }

    public void setFigure(T figure) {
        this.figure = figure;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("Message{");
        sb.append("figure=").append(figure);
        sb.append('}');
        return sb.toString();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Message<?> message = (Message<?>) o;

        return figure != null ? figure.equals(message.figure) : message.figure == null;
    }

    @Override
    public int hashCode() {
        return figure != null ? figure.hashCode() : 0;
    }
}
