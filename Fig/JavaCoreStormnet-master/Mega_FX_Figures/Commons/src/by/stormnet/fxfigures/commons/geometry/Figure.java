package by.stormnet.fxfigures.commons.geometry;

import javafx.scene.canvas.GraphicsContext;
import javafx.scene.paint.Color;

import java.io.Serializable;


public abstract class Figure implements Printable, Squerable, Serializable {
    private static final long serialVersionUID = 2L;

    public static final int FIGURE_TYPE_CIRCLE      = 0;
    public static final int FIGURE_TYPE_RECTANGLE   = 1;
    public static final int FIGURE_TYPE_TRIANGLE    = 2;

    private int type;

    protected double cX;
    protected double cY;
    protected double lineWidth;
    protected transient Color color;

    public Figure(int type, double cX, double cY, double lineWidth, Color color) {
        this.type = type;
        this.cX = cX;
        this.cY = cY;
        this.lineWidth = lineWidth;
        this.color = color;
    }

    public int getType() {
        return type;
    }

    public double getcX() {
        return cX;
    }

    public void setcX(double cX) {
        this.cX = cX;
    }

    public double getcY() {
        return cY;
    }

    public void setcY(double cY) {
        this.cY = cY;
    }

    public double getLineWidth() {
        return lineWidth;
    }

    public void setLineWidth(double lineWidth) {
        this.lineWidth = lineWidth;
    }

    public Color getColor() {
        return color;
    }

    public void setColor(Color color) {
        this.color = color;
    }

    public abstract void draw(GraphicsContext gc);

    @Override
    public void printInfo() {

    }


}
