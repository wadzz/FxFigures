package by.stormnet.fxfigures.commons.geometry;

public interface Printable {
    void printInfo();
}
