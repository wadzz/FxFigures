package by.stormnet.fxfigures.commons.geometry;

import javafx.scene.canvas.GraphicsContext;
import javafx.scene.paint.Color;

public class Triangle extends Figure {
    private double base;

    public Triangle(double cX, double cY, double lineWidth, Color color, double base) {
        this(cX, cY, lineWidth, color);
        this.base = base;
    }

    private Triangle(double cX, double cY, double lineWidth, Color color) {
        super(FIGURE_TYPE_TRIANGLE, cX, cY, lineWidth, color);

    }

    public double getBase() {
        return base;
    }

    public void setBase(double base) {
        this.base = base;
    }

    @Override
    public void draw(GraphicsContext gc) {
        gc.setStroke(color);
        gc.setLineWidth(lineWidth);
        gc.strokeLine(cX - base/2, cY + base/2, cX + base/2, cY + base/2);
        gc.strokeLine(cX + base/2, cY + base/2, cX, cY - base/2);
        gc.strokeLine(cX, cY - base/2, cX - base/2, cY + base/2);

    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("Triangle{");
        sb.append("base=").append(base);
        sb.append(", cX=").append(cX);
        sb.append(", cY=").append(cY);
        sb.append(", lineWidth=").append(lineWidth);
        sb.append(", color=").append(color);
        sb.append('}');
        return sb.toString();
    }

    @Override
    public void printInfo() {
        System.out.println(toString());
    }

    @Override
    public double printSquare() {
        return (1/2 * base * base);
    }
}
