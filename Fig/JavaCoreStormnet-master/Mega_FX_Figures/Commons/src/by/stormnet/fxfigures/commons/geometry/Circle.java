package by.stormnet.fxfigures.commons.geometry;

import javafx.scene.canvas.GraphicsContext;
import javafx.scene.paint.Color;

public class Circle extends Figure {

    private double radius;

    public Circle(double cX, double cY, double lineWidth, Color color, double radius) {
        this(cX, cY, lineWidth, color);
        this.radius = radius;
    }

    private Circle(double cX, double cY, double lineWidth, Color color) {
        super(Figure.FIGURE_TYPE_CIRCLE, cX, cY, lineWidth, color);
    }



    public double getRadius() {
        return radius;
    }

    public void setRadius(double radius) {
        this.radius = radius;
    }

    @Override
    public void draw(GraphicsContext gc) {
        gc.setStroke(color);
        gc.setLineWidth(lineWidth);
        gc.strokeOval(cX - radius, cY - radius, radius * 2, radius * 2);

    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("Circle{");
        sb.append("radius=").append(radius);
        sb.append(", cX=").append(cX);
        sb.append(", cY=").append(cY);
        sb.append(", lineWidth=").append(lineWidth);
        sb.append(", color=").append(color);
        sb.append('}');
        return sb.toString();
    }

    @Override
    public void printInfo() {
        System.out.println(toString());
    }

    @Override
    public double printSquare() {
        return (getRadius()*getRadius()* Math.PI);
    }
}
