package by.stormnet.fxfigures.commons.messaging;

import by.stormnet.fxfigures.commons.threading.IClientThread;

public interface MessageDispatcher<T extends Thread & IClientThread> {
    void registerClientThread(T client);
    void unregisterClientThread(T client);
   <M extends Message> void dispatchMessage(M message);
    void  clear();
}
