package by.stormnet.fxfigures.server;

import java.io.IOException;
import java.util.Scanner;

public class Application {
    private Scanner scanner;
    private boolean serverStarter;

    public static void main(String[] args) {
        new Application().runTheApp();
    }

    private void runTheApp() {
        while (!serverStarter){
            try {
                Server server = new Server(readTheUserInput("Enter port's number (1025-65535)"));
                server.start();
                serverStarter = true;
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
    private int readTheUserInput(String string){
        System.out.println(string);
        scanner = new Scanner(System.in);
        if (scanner.hasNextInt()){
            return scanner.nextInt();
        }
        System.out.println("Wrong value, Try again");
        return  readTheUserInput(string);
    }
}
