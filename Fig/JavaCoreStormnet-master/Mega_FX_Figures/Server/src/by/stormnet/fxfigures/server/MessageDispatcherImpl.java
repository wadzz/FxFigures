package by.stormnet.fxfigures.server;

import by.stormnet.fxfigures.commons.messaging.Message;
import by.stormnet.fxfigures.commons.messaging.MessageDispatcher;
import by.stormnet.fxfigures.commons.threading.IClientThread;

import java.io.IOException;
import java.util.LinkedList;
import java.util.List;

public class MessageDispatcherImpl<T extends Thread & IClientThread> implements MessageDispatcher<T> {

    private List<T> clients;

    public MessageDispatcherImpl() {
        clients = new LinkedList<>();
    }

    @Override
    public void unregisterClientThread(T client) {
        client.stopThread();
        if (clients.contains(client)){
            clients.remove(client);
        }
    }

    @Override
    public void registerClientThread(T client) {
        client.setClientThreadListener(this);
        clients.add(client);
        client.start();
    }

    @Override
    public <M extends Message> void dispatchMessage(final M message) {
        clients.forEach(m ->{
            try {
                m.sendMessage(message);
            } catch (IOException e) {
                e.printStackTrace();
            }
        });
    }

    @Override
    public void clear() {
        clients.forEach(client -> client.stopThread());
        clients.clear();
    }
}
