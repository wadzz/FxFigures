package by.stormnet.fxfigures.server;

import by.stormnet.fxfigures.commons.messaging.MessageDispatcher;
import by.stormnet.fxfigures.commons.threading.ClientThread;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;

public class Server extends Thread {
    private boolean isRunning;

    private ServerSocket serverSocket;
    private MessageDispatcher<ClientThread> msgDispatcher;

    public Server(int port) throws IOException {
        serverSocket = new ServerSocket(port);
        msgDispatcher = new MessageDispatcherImpl<>();
    }

    @Override
    public void run() {
        isRunning = true;
        System.out.println("Server started at port " + serverSocket.getLocalPort());
        while (isRunning){
            try {
                Socket socket = serverSocket.accept();
                ClientThread client = new ClientThread(socket);
                msgDispatcher.registerClientThread(client);
                System.out.println("New client connected!");
                Thread.sleep(1l);
            } catch (IOException e) {
                e.printStackTrace();
            } catch (InterruptedException e) {
                e.printStackTrace();
                isRunning = false;
            }
        }
        try {
            serverSocket.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        msgDispatcher.clear();
    }
}
